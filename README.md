# Word-Guess-Game

### This hangman-style Spongebob themed game was built with Javascript, jQuery, and Bootstrap.

![SpongeBob](assets/images/spongebob.png)

### Instructions:
* Press any key to start the game.
* Guess a character name or place letter by letter.
* Incorrect guesses will appear under the selected word. Correct guesses will replace the _'s.
* Guesses left decrease by 1 for each incorrect guess.
* A message appears beneath the game instructions when the correct word is guessed or if the number of remaining guesses reaches 0. Wins and losses are tallied below.

### Opening Page
![Word Guess Game Opening Page](assets/images/first-page.png)

### Game Starts
![Word Guess Game Game Started](assets/images/started.png)

### User Guesses
![Word Guess Game User Guesses](assets/images/guessed.png)

### User Wins
![Word Guess Game User Wins](assets/images/won.png)

### User Lost
![Word Guess Game User Lost](assets/images/lost.png)


[Deployed Site](https://zoe-gonzales.github.io/Word-Guess-Game/)

### Additional
* User can play and pause the Spongebob theme song.
