// document.ready function
$(document).ready(function (){
    
    // Global variables
    var wordToGuess = [
        "gary", 
        "squidward", 
        "patrick", 
        "spongebob", 
        "sandy", 
        "pearl", 
        "mrkrabs", 
        "plankton",
        "squarepants",
        "krabbypatty",
        "krustykrab",
        "chumbucket",
        "bikinibottom",
        "flyingdutchman",
        "mermaidman",
        "barnacleboy"
    ];
    var currentWord = "";
    var letters = [];
    var blanks = 0;
    var blanksAndCorrectGuesses = [];
    var wrongGuesses = [];
    var result;
    
    // Counter variables
    var winCounter = 0;
    var lossCounter = 0;
    var guessesLeft = 12;

    // Prevents game from running until user presses a key
    // var gameStarted = false;

    // Games Sounds
    // Theme Song 
    var SpongeBobTheme = document.createElement("audio");
    SpongeBobTheme.setAttribute("src", "./assets/spongebob-theme.mp3");
    
    // Sound when user wins
    var winSoundEffect = document.createElement("audio");
    winSoundEffect.setAttribute("src", "./assets/win.mp3");

    // Sound when user loses
    var loseSoundEffect = document.createElement("audio");
    loseSoundEffect.setAttribute("src", "./assets/lose.mp3");

    // Theme Song plays or pauses on click
    $("#play-theme").on("click", function (){
        SpongeBobTheme.play();
    });
    $("#pause-theme").on("click", function (){
        SpongeBobTheme.pause();
    });

    var WordGuessGame = {
        gameStarted: false,
        // Function runs when user presses any key, game is started
        startGame: function () {
            gameStarted = true;
            var gameInstructions = document.getElementById("key-start");
            gameInstructions.innerHTML = "Guess the word below." + "<br>" + "You have only 12 guesses, so choose wisely!";
        },
        // Function displays letters to guess as _'s
        playGame: function () {
            // Computer selects random word from array
            currentWord = wordToGuess[Math.floor(Math.random() * wordToGuess.length)];
            var letters = currentWord.split("");
            blanks = letters.length;

            // Reset game
            guessesLeft = 12;
            blanksAndCorrectGuesses = [];
            wrongGuesses = [];
            
            // Loop through blanksAndCorrectGuess
            for (i = 0; i < blanks; i++) {
                blanksAndCorrectGuesses.push("_");
            }

            $("#word").text(blanksAndCorrectGuesses.join(" "));
            $("#guesses-left").text(guessesLeft);
            $("#wins").text(winCounter);
            $("#losses").text(lossCounter);
        },
        // Function verifies if word exists in word and replaces _ with letter
        checkUserGuess: function (char) {
            // Checks if character exists in current word
            var charExistsInWord = false;
            for (var i = 0; i < blanks; i++) {
                if (currentWord[i] == char) {
                    charExistsInWord = true;
                }
            }
            // check if character matches letter in word
            if (charExistsInWord) {
                for (var i = 0; i < blanks; i++ ) {
                    if (currentWord[i] == char) {
                        blanksAndCorrectGuesses[i] = char;
                    }
                }
            } else {
            // For each user guess, number of remaining guesses goes down by one
                if(wrongGuesses.includes(char)) {
                    return;
                } else {
                    guessesLeft--;
                    wrongGuesses.push(char.toString());
                }
            }
        },
        // Function resets game once user guesses currentWord or guesses left = 0
        gameOver: function () {
            $("#guesses-left").text(guessesLeft);
            $("#word").text(blanksAndCorrectGuesses.join(" "));
            // Display the letters already guessed
            $("#guesses").text(wrongGuesses.join(" "));
            // Checks if user guessed all letters correctly
            letters = currentWord.split("");
            if (letters.toString() == blanksAndCorrectGuesses.toString()) {
                winCounter++;
                $("#result").html("<h2> Aye captain, you won! </h2>");
                $("#result").addClass("text-center result");
                winSoundEffect.play();
                $("#wins").text(winCounter);
                this.playGame();
            // Checks if user lost
            } else if (guessesLeft === 0) {
                lossCounter++;
                $("#result").html("<h2> Oh tartar sauce, you lost. </h2>");
                $("#result").addClass("text-center");
                loseSoundEffect.play();
                $("#losses").text(lossCounter);
                this.playGame();
            }
        }
    }
 
    // Game starts when user presses a key
    document.onkeyup = function(event) {
        WordGuessGame.startGame();
        WordGuessGame.playGame();
        // Saves user input and runs function to check userGuess
        document.onkeyup = function(event) {
        $("#result").empty();
        var userGuess = event.key.toLowerCase();
        WordGuessGame.checkUserGuess(userGuess);
        // Resets game
        WordGuessGame.gameOver();
        }
    }
});
